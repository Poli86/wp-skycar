'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var size = require('gulp-size');
var csso = require('gulp-csso');
 
var browserSync = require('browser-sync').create();
var autoprefixer = require('gulp-autoprefixer');
 
sass.compiler = require('node-sass');

gulp.task('sass', function () {
  return gulp.src('./assets/src/scss/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(size())
    .pipe(autoprefixer({ cascade: false }))
    .pipe(csso())
    .pipe(size())
    .pipe(gulp.dest('./assets/dist/css'))
    .pipe(browserSync.stream())
    ;
});
 

gulp.task('dev', function() {
    browserSync.init({
        proxy: "skycars.local"
    });

    gulp.watch("./assets/src/scss/**/*.scss", gulp.series('sass'));
    gulp.watch("./**/*.php").on('change', browserSync.reload);
});

gulp.task("default", gulp.series('sass', 'dev'));