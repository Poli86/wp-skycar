<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package skycar
 */

?>

	</div>

	<footer id="colophon" class="site-footer">

		<?php
		wp_nav_menu( array( 'theme_location' => 'menu-2', 'menu_id'  => 'footer-menu' ) )?>
		<div class="footer-business-info">
			<h4>SKYCAR DETAILING</h4>
			<h6>"Milen Kamak" 65, 1000 Sofia Center,</h6>
			<h6><a href="/">+359 889 888 888</a></h6>
			<h6><a href="/">Atanasov@skycardetailing.com</a></h6>
	    </div>
		<p>All right reserved &copy;2016 - <?php echo date('Y'); ?><?php echo bloginfo('name'); ?></p>
	</footer>
</div>

<?php wp_footer(); ?>

</body>
</html>
