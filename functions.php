<?php
/**
 * skycar functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package skycar
 */

if ( ! function_exists( 'skycar_setup' ) ) :
	
	function skycar_setup() {
		
		load_theme_textdomain( 'skycar', get_template_directory() . '/languages' );

		
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'html5', array(' search-form', 'comment-form', 'comment-list', 'gallery', 'caption') );

		
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'skycar' ),
			'menu-2' => esc_html__( 'Footer', 'skycar' ),
		) );

	}
endif;
add_action( 'after_setup_theme', 'skycar_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function skycar_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'skycar_content_width', 640 );
}
add_action( 'after_setup_theme', 'skycar_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function skycar_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'skycar' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'skycar' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'skycar_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function skycar_scripts() {
	wp_enqueue_style( 'skycar-style', get_template_directory_uri() . '/assets/dist/css/theme.css' );

	// wp_enqueue_script( 'skycar-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

}
add_action( 'wp_enqueue_scripts', 'skycar_scripts' );

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';




